
drop database db5;


create database db5;

use db5;



CREATE TABLE Users

(
	
	User_ID int Primary Key,
	
	User varchar(100)

);




CREATE TABLE App

(
	
	App_ID int Primary Key,
	
	AppName varchar(250)

);




CREATE TABLE MAC

(
	
	MAC_ID varchar(20) Primary Key

);




CREATE TABLE Reports

(
	
	Identifier varchar(100) Primary Key,
	
	App_ID int,
	User_ID int,
	
	MAC_ID varchar(20),
	
	Foreign Key (App_ID) REFERENCES App(App_ID),
	
	Foreign Key (User_ID) REFERENCES Users(User_ID),
	
	Foreign Key (MAC_ID) REFERENCES MAC(MAC_ID),
	
	Dates Date,
	
	Times Time
	

);


