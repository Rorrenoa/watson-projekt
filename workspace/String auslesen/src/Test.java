
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.NetworkInterface;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		
		
		for ( NetworkInterface ni : Collections.list( NetworkInterface.getNetworkInterfaces() ) ) {
			  byte[] adr = ni.getHardwareAddress();
			  if ( adr == null || adr.length != 6 )
			    continue;
			  String mac = String.format( "%02X:%02X:%02X:%02X:%02X:%02X",
			                                    adr[0], adr[1], adr[2], adr[3], adr[4], adr[5] );
			  System.out.println( mac );
			}

		File file = new File("file:////C:/");
		System.out.println(file.listFiles() );

	}

	public static List<File> searchFile(File dir, String find) {

		File[] files = dir.listFiles();
		List<File> matches = new ArrayList<File>();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {

				if (files[i].getName().equalsIgnoreCase(find)) {
					matches.add(files[i]);
				}
				if (files[i].isDirectory()) {
					matches.addAll(searchFile(files[i], find));
				}
			}
		}
		return matches;
	}

	/*
	 * public static String readstring(String finde) { String text = new
	 * String();
	 * 
	 * try (BufferedReader br = new BufferedReader(new FileReader(
	 * "T:/11_Klasse/AWP_STE/Maria/AppCrash_AD2F1837.HPPrint_a2b37f56cd62649abb201aeb83588f10b75686ee_10324e52_1ccea56a/Report.wer"
	 * ))) {
	 * 
	 * String sCurrentLine = new String(); while ((sCurrentLine = br.readLine())
	 * != null) { String sCurrentLiness =
	 * sCurrentLine.replaceAll("[^\\p{Print}]", ""); text += sCurrentLiness;
	 * text += "\n";
	 * 
	 * }
	 * 
	 * } catch (IOException e) { e.printStackTrace(); }
	 * System.out.println(text);
	 * 
	 * }
	 */

}
